# ReactRecipeBox
This is work is the full work that was done for a challenge at [FreeCodeCamp](https://www.freecodecamp.com/baffledbear). Specifically, the [Recipe Box challenge](https://www.freecodecamp.com/challenges/build-a-recipe-box).

I implemented this using React Bootstrap, which implements Twitter's Bootstrap CSS framework for Facebook's React.js framework. 
