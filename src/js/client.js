import Accordion from 'react-bootstrap/lib/Accordion';
import Button from 'react-bootstrap/lib/Button';
import Col from 'react-bootstrap/lib/Col';
import Grid from 'react-bootstrap/lib/Grid';
import Input from 'react-bootstrap/lib/Input';
import ListGroup from 'react-bootstrap/lib/ListGroup';
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem';
import Modal from 'react-bootstrap/lib/Modal';
import Panel from 'react-bootstrap/lib/Panel';
import React from 'react';
import ReactDOM from 'react-dom';
import Row from 'react-bootstrap/lib/Row';
import Well from 'react-bootstrap/lib/Well';

if (typeof window !== 'undefined') {
    window.React = React;
}

const recipeTitleStyle = {
    marginTop: '0px'
};

const buttonStyle = {
    marginLeft: '5px'
};

const addButtonStyle = {
    marginLeft: '20px',
    marginTop: '20px'
}

var recipeArr = (typeof localStorage["recipeBox"] !=
                 "undefined") ?
                    JSON.parse(localStorage["recipeBox"]) :
                    [
                        {
                            title: "Pumpkin Pie",
                            ingredientList: [
                                "Pumpkin Puree",
                                "Sweetened Condensed Milk",
                                "Eggs",
                                "Pumpkin Pie Spice",
                                "Pie Crust"
                                ]
                        },
                        {
                            title: "Spaghetti",
                            ingredientList: [
                                "Noodles",
                                "Tomato Sauce",
                                "(Optional) Meatballs"
                                ]
                        },
                        {
                            title: "Onion Pie",
                            ingredientList: [
                            "Onion",
                            "Pie Crust",
                            "Sounds Yummy right?"
                            ]
                        }
                    ];

class Configuration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ingredients: this.props.ingredients,
            title: this.props.recipeTitle
        };
    }

    handleTitleChange(e) {
        this.setState({title: e.target.value});
    }

    handleIngredientChange(e) {
        this.setState({ingredients: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        var title = this.state.title.trim();
        var ingredients = this.state.ingredients;
        if(typeof ingredients === "string") {
            ingredients = ingredients.split(",");
        }
        if(!ingredients || !title) {
            return;
        }
        this.props.saveModal({
            title: title,
            ingredients: ingredients
        });
        this.props.onHide();
    }

    render() {
        return (
                <Modal
                show={this.props.show}
                onHide={this.props.onHide}>

                    <Modal.Header closeButton>
                        <h3>Configure Recipe</h3>
                    </Modal.Header>
                    <form className="configForm">
                    <Modal.Body>

                        <Input
                        type="text"
                        placeholder="Recipe Title"
                        value={this.state.title}
                        onChange={
                            this.handleTitleChange.bind(this)
                        } />

                        <Input
                        type="textarea"
                        placeholder="Ingredients"
                        value={this.state.ingredients}
                        onChange={this.handleIngredientChange.bind(this)} />

                    </Modal.Body>
                    <Modal.Footer>

                        <Button onClick={this.handleSubmit.bind(this)}>
                        Save
                        </Button>

                        <Button onClick={this.props.onHide}>
                        Close
                        </Button>

                    </Modal.Footer>
                    </form>
                </Modal>
            );
    }
}

class RecipeBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            recipes: recipeArr,
            showModal: false
        };
    }

    handleRecipesUpdate() {
        this.setState({recipes: recipeArr});
    }

    closeModal() {
        this.setState({showModal: false});
    }

    openModal() {
        this.setState({showModal: true});
    }

    saveModal(e) {
        var ingredients = e.ingredients;
        var title = e.title;
        recipeArr.push({
            title: title,
            ingredientList: ingredients
        });
        localStorage.setItem('recipeBox', JSON.stringify(recipeArr));
        this.handleRecipesUpdate();
    }

    render() {
        return (
            <Grid style={{marginTop: '20vh'}}>
                <Well>
                    <Configuration
                    id={this.props.id}
                    recipeTitle={this.state.title}
                    ingredients={this.state.ingredients}
                    show={this.state.showModal}
                    onHide={this.closeModal.bind(this)}
                    saveModal={this.saveModal.bind(this)}/>

                    <RecipeList
                    recipes={this.state.recipes}
                    handleUpdate={this.handleRecipesUpdate.bind(this)} />

                    <Button
                    bsStyle="primary"
                    style={addButtonStyle}
                    onClick={this.openModal.bind(this)}>
                    Add
                    </Button>

                </Well>
            </Grid>
        );
    }
}

class RecipeList extends React.Component {
    render() {
        var recipeNodes = this.props.recipes.map(
            function(recipe, index) {
            return (
                <Panel
                header={recipe.title}
                style={recipeTitleStyle}
                id={"panel-"+index}
                key={index}
                eventKey={index}
                >
                    <Recipe
                    recipeTitle={recipe.title}
                    ingredients={recipe.ingredientList}
                    id={index}
                    handleUpdate={this.props.handleUpdate} />
                </Panel>
            );
        }.bind(this));
        return (
            <Accordion>
                {recipeNodes}
            </Accordion>
            );
    }
}

class Recipe extends React.Component {
    constructor(props) {
        super(props);
        this.bool = false;
        this.state = {
            showModal: false,
            ingredients: this.props.ingredients,
            title: this.props.recipeTitle
        };
    }

    saveModal(e) {
        var ingredients = e.ingredients;
        var title = e.title;
        this.setState({
            title: title,
            ingredients: ingredients
        });
        recipeArr[this.props.id].title = title;
        recipeArr[this.props.id].ingredientList = ingredients;
        localStorage.setItem('recipeBox', JSON.stringify(recipeArr));
        this.props.handleUpdate();
    }

    deleteItem() {
        recipeArr.splice(this.props.id, 1);
        localStorage.setItem('recipeBox', JSON.stringify(recipeArr));
        var panel = document.getElementById("panel-" + this.props.id);
        panel.parentElement.style.display = "none";
    }

    closeModal() {
        this.setState({showModal: false});
    }

    openModal() {
        this.setState({showModal: true});
    }

    render() {
        return (
                <div className="recipe">
                    <Configuration
                    id={this.props.id}
                    recipeTitle={this.state.title}
                    ingredients={this.state.ingredients}
                    show={this.state.showModal}
                    onHide={this.closeModal.bind(this)}
                    saveModal={this.saveModal.bind(this)}/>

                    <IngredientList
                    openModal={this.openModal.bind(this)}
                    title={this.state.title}
                    ingredients={this.state.ingredients}
                    />

                    <Button
                    bsStyle="danger"
                    style={buttonStyle}
                    onClick={this.deleteItem.bind(this)}>
                    Delete
                    </Button>

                    <Button
                    bsStyle="default"
                    style={buttonStyle}
                    onClick={this.openModal.bind(this)}>
                    Edit
                    </Button>
                </div>
            );
    }
}

class IngredientList extends React.Component {

    render() {
        var ingredientList = this.props.ingredients.map(
            function(ingredient, index) {
            return (
                <Ingredient
                key={ingredient+index}
                ingredient={ingredient} />
            );
        });
        return (
                <Row>
                    <Col lg={12} md={12}>
                            <IngredientTitle />
                            <ListGroup>
                                {ingredientList}
                            </ListGroup>
                    </Col>
                </Row>
            );
    }
}

class IngredientTitle extends React.Component {
    render() {
        return (
                <Row className="text-center">
                    <h3>Ingredients</h3>
                </Row>

            );
    }
}

class Ingredient extends React.Component {
    render() {
        return <ListGroupItem>{this.props.ingredient}</ListGroupItem>;
    }
}

const recipeBox = (
    <Grid style={{marginTop: '20vh'}}>
        <Well>
            <RecipeList />
            <Button bsStyle="primary" style={addButtonStyle}>Add</Button>
        </Well>
    </Grid>
    );

ReactDOM.render(
        <RecipeBox />,
        document.getElementById("recipes")
    );
